import { Request, Response, NextFunction } from 'express';

export const handler = async (req: Request, res: Response, next: NextFunction) => {
  res.json({ hello: 'World!' });
};

export default handler;
