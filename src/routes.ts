import { Router } from 'express';
import helloWorld from './controllers/helloWorldController';

export default () => {
  const api = Router();
  api.get('/', helloWorld);
  return api;
};
