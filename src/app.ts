import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import devErrorHandler from 'errorhandler';
import productionErrorHandler from './middleware/json-error-handler';
import checkJwt from './middleware/jwt-auth';
import validator from 'express-validator';
import * as mongo from './services/mongoose';
import routes from './routes';

mongo.connect();
const app = express();

app.set('port', process.env.PORT || 3000);
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(checkJwt);
app.use(validator());

app.use('/v1', routes());

const errorHandler = app.get('env') === 'development' ? devErrorHandler() : productionErrorHandler();
app.use(errorHandler);

export default app;
