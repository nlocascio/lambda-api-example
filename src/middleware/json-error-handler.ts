import { Request, Response, NextFunction } from 'express';

const handler = () => (err: any, req: Request, res: Response, next: NextFunction) => {
  res.status(err.status || 500).json({ error: err.message });
};

export default handler;
