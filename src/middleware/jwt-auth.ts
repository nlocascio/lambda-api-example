import jwt from  'express-jwt';
import jwksRsa from  'jwks-rsa';
import { AUTH0_DOMAIN, AUTH0_API_IDENTIFIER } from '../services/secrets';

const checkJwt = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://${AUTH0_DOMAIN}/.well-known/jwks.json`
  }),
  audience: AUTH0_API_IDENTIFIER,
  issuer: `https://${AUTH0_DOMAIN}/`,
  algorithms: ['RS256']
});

export default checkJwt;
