import mongoose from 'mongoose';
import { MONGODB_URI, ENVIRONMENT } from './secrets';

const mongoUrl = MONGODB_URI;
const autoIndex = 'production' !== ENVIRONMENT;
const config = {
  autoIndex
};

export function connect() {
  mongoose.set('useCreateIndex', true);
  mongoose
    .connect(mongoUrl, { config })
    .then(() => console.log('MongoDB connection established.'))
    .catch(err => {
      console.log('MongoDB connection error. Please make sure MongoDB is running. ' + err);
      process.exit();
      });
}

export default mongoose;
