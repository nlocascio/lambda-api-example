import logger from './logger';
import dotenv from 'dotenv';
import fs from 'fs';

if (fs.existsSync('.env')) {
    logger.debug('Using .env file to supply config environment variables');
    dotenv.config({ path: '.env' });
}

export const ENVIRONMENT = process.env.NODE_ENV;

export const MONGODB_URI = process.env['MONGODB_URI'];

if (!MONGODB_URI) {
    logger.error('No mongo connection string. Set MONGODB_URI environment variable.');
    process.exit(1);
}

export const AUTH0_DOMAIN = process.env.AUTH0_DOMAIN;
export const AUTH0_API_IDENTIFIER = process.env.AUTH0_API_IDENTIFIER;
export const MINDBODY_SOURCE_NAME = process.env.MINDBODY_SOURCE_NAME;
export const MINDBODY_SOURCE_PASSWORD = process.env.MINDBODY_SOURCE_PASSWORD;
