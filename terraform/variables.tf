variable "APP_VERSION" {
  default = "0.0.6"
}

variable "aws_region" {
    description = "EC2 Region for the VPC"
    default = "us-east-1"
}
