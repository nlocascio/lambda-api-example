#!/bin/sh
rm -rf dist app.zip
npm run build
npm prune --production
zip -rq ./app.zip node_modules dist lambda.js
aws s3 cp app.zip s3://nlsw/v0.0.6/app.zip
